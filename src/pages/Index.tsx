/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Link } from "@react-navigation/native";
import React from "react";
import { View } from "react-native";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export const Index = (props) => {
  // form field values
  const _formData = {
    username: "Saurabh",
    password: "Password@12",
  };

  // // form schema
  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      username: { type: "string" },
      password: { type: "string" },
    },
  });

  //   const RoleLogin = (props) => {
  //       if (_formData.username && _formData.password === "")
  //       {
  //           return(
  //               props.navigation.navigate("Profile")
  //           )
  //       }
  //       else if (_formData.username && _formData.password == "ServiceTaker")
  //       {
  //           return(
  //               props.navigation.navigate("Login")
  //           )
  //       }
  //       else if (_formData.username && _formData.password == "Staff")
  //       {
  //           return(
  //               props.navigation.navigate("Verification")
  //           )
  //       }
  //       else
  //       {
  //           return(
  //               <Text>Please check your details </Text>
  //           )
  //       }
  //     }

  return (
    <View>
      <JsonForm schema={_schema} _formData={_formData} />
      {/* <Button title='submit' onPress = {() => {RoleLogin(props)}}> */}
      <Link
        style={{
          backgroundColor: "blue",
          width: 50,
          height: 50,
          color: "white",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: 50,
        }}
        to="/First"
      >
        Go
      </Link>
    </View>
  );
};
