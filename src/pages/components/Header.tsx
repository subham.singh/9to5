/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Link } from "@react-navigation/native";
import React from "react";
import { View } from "react-native";

export default function Header() {
  return (
    <View>
      <Link style={{ color: "green", fontSize: 20 }} to="/Index">
        Initial App
      </Link>
      <Link style={{ color: "green", fontSize: 20 }} to="/First">
        Home
      </Link>
      <Link style={{ color: "green", fontSize: 20 }} to="/Alternate">
        Alternate
      </Link>
      <Link style={{ color: "green", fontSize: 20 }} to="/Test">
        Test
      </Link>
      <Link style={{ color: "green", fontSize: 20 }} to="/Profile">
        Admin
      </Link>
      <Link style={{ color: "green", fontSize: 20 }} to="/Login">
        Service Taker
      </Link>
      <Link style={{ color: "green", fontSize: 20 }} to="/Verification">
        Verification
      </Link>
    </View>
  );
}
