/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Button,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

const DATA = [
  {
    id: "1",
    description: "PNR :825084\n ",
  },
];

const Item = ({ item, onPress, style }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Text style={styles.description}>{item.description}</Text>
  </TouchableOpacity>
);

export const Confirmation = (props) => {
  const [paymentDone, setPaymentDone] = useState(false);
  const [selectedId, setSelectedId] = useState(null);
  const qrUrl =
    "https://www.qr-code-generator.com/wp-content/themes/qr/new_structure/markets/core_market_full/generator/dist/generator/assets/images/websiteQRCode_noFrame.png";

  useEffect(() => {
    setTimeout(() => setPaymentDone(true), 3000);
  }, []);

  if (!paymentDone)
    return (
      <ActivityIndicator size="large" color="black" style={styles.horizontal} />
    );

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "yellow" : "white";

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        style={{ backgroundColor }}
      />
    );
  };

  return (
    <>
      <View style={{ justifyContent: "center", flex: 1, alignItems: "center" }}>
        <Image
          source={{
            uri: qrUrl,
          }}
          style={{ height: 400, width: 300 }}
        />
        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
        />
      </View>
      <View style={styles.Btns}>
        <Button
          title="Back to Home"
          onPress={() => {
            props.navigation.navigate("First");
          }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 2,
    marginVertical: 6,
    marginHorizontal: 8,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#D3D6D6",
    backgroundColor: "#FFFFFF",
    opacity: 1,
    borderRadius: 2,
  },
  description: {
    fontSize: 18,
    color: "red",
    textAlign: "left",
  },
  horizontal: {
    justifyContent: "center",
    padding: 10,
    alignItems: "center",
    flex: 1,
  },
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
