/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";

export const Test = () => {
  const { goBack } = useNavigation();

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Test</Text>
      <Button title="👈 Go back" onPress={() => goBack()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 16,
  },
});
