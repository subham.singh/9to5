/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React, { useState, useEffect } from "react";
import {
  FlatList,
  View,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  Button,
  ActivityIndicator,
} from "react-native";

const DATA = [
  {
    id: "1",
    title: "Confirmation\n\n\n",
    description:
      "Booking has been confirmed \n\nBooking id : BKId0214\nTransaction Id : TID8456\n\n\n",
    subInfo: "Thank you choosing our platform for booking.",
  },
];

const Item = ({ item, onPress, style }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Text style={styles.title}>{item.title}</Text>
    <Text style={styles.description}>{item.description}</Text>
    <Text style={styles.subInfo}>{item.subInfo}</Text>
  </TouchableOpacity>
);

export const PreConfirmation = (props) => {
  const [selectedId, setSelectedId] = useState(null);
  const [paymentDone, setPaymentDone] = useState(false);

  useEffect(() => {
    setTimeout(() => setPaymentDone(true), 3000);
  }, []);

  if (!paymentDone)
    return (
      <ActivityIndicator size="large" color="black" style={styles.horizontal} />
    );

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "yellow" : "white";

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        style={{ backgroundColor }}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ justifyContent: "center", flex: 1 }}>
        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
        />
      </View>
      <View style={{ alignItems: "center" }}>
        <Button
          title="Submit"
          onPress={() => {
            props.navigation.navigate("Confirmation");
          }}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#D3D6D6",
    backgroundColor: "#FFFFFF",
    opacity: 1,
    borderRadius: 2,
  },
  title: {
    fontSize: 45,
    color: "black",
    fontWeight: "bold",
    textAlign: "center",
  },
  description: {
    fontSize: 34,
    color: "black",
    textAlign: "left",
  },
  subInfo: {
    fontSize: 28,
    color: "green",
    textAlign: "center",
  },
  horizontal: {
    justifyContent: "center",
    padding: 10,
    alignItems: "center",
    flex: 1,
  },
});
