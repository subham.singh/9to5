/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { Calendar } from "react-native-calendars";

export const Cal = (props) => {
  return (
    <View style={{ justifyContent: "center", flex: 1 }}>
      <Calendar
        style={{
          height: 350,
          margin: 20,
        }}
        theme={{
          backgroundColor: "#ffffff",
          calendarBackground: "#ffffff",
          textSectionTitleColor: "black",
          textSectionTitleDisabledColor: "#d9e1e8",
          selectedDayBackgroundColor: "orange",
          selectedDayTextColor: "blue",
          todayTextColor: "#00adf5",
          dayTextColor: "#2d4150",
          textDisabledColor: "#d9e1e8",
          dotColor: "#00adf5",
          selectedDotColor: "green",
          arrowColor: "black",
          disabledArrowColor: "black",
          monthTextColor: "black",
          indicatorColor: "black",
          textDayFontFamily: "monospace",
          textMonthFontFamily: "monospace",
          textDayHeaderFontFamily: "monospace",
          textDayFontWeight: "300",
          textMonthFontWeight: "bold",
          textDayHeaderFontWeight: "300",
          textDayFontSize: 16,
          textMonthFontSize: 16,
          textDayHeaderFontSize: 16,
        }}
        minDate={"2020-12-01"}
        maxDate={"2021-01-30"}
        onDayPress={(day) => {
          console.log("selected day", day);
        }}
        onDayLongPress={(day) => {
          console.log("selected day", day);
        }}
        monthFormat={"MMM yyyy"}
        onMonthChange={(month) => {
          console.log("month changed", month);
        }}
        hideExtraDays={true}
        disableMonthChange={true}
        firstDay={1}
        hideDayNames={false}
        showWeekNumbers={false}
        onPressArrowLeft={(subtractMonth) => subtractMonth()}
        onPressArrowRight={(addMonth) => addMonth()}
        markedDates={{
          "2020-12-13": { marked: true },
          "2020-12-14": { marked: true },
          "2020-12-15": { marked: true, dotColor: "red", activeOpacity: 0 },
          "2020-12-16": { disabled: true, disableTouchEvent: true },
          "2020-12-19": { marked: true },
          "2020-12-25": { marked: true },
        }}
      />
      <View style={styles.Btns}>
        <Button
          title="Continue"
          onPress={() => {
            props.navigation.navigate("TimeSlot");
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
