/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, ScrollView, StyleSheet, View } from "react-native";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export const GenerateWidget = (props) => {
  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      color: {
        enum: ["Red", "Blue", "Green"],
        type: "string",
      },
      backgroundC: {
        enum: ["White", "Black"],
        type: "string",
      },
      fonts: {
        enum: [
          "Arial",
          "Helvetica",
          "Times New Roman",
          "Times Courier New ",
          "Courier",
        ],
        type: "string",
      },
      width: {
        type: "number",
      },
      height: {
        type: "number",
      },
    },
  });

  const uiSchema = {
    color: {
      "ui:title": "Color",
      "ui:placeholder": "Please select",
      "ui:widget": "select",
    },
    backgroundC: {
      "ui:title": "Background Color",
      "ui:placeholder": "Please select",
      "ui:widget": "select",
    },
    fonts: {
      "ui:title": "Font",
      "ui:placeholder": "Please select",
      "ui:widget": "select",
    },
    width: {
      "ui:title": "Width",
    },
    height: {
      "ui:title": "Height",
    },
  };

  return (
    <ScrollView>
      <JsonForm
        schema={_schema}
        uiSchema={uiSchema}
        _onSuccess={(e) => {
          props.navigation.navigate("BookingListingAdmin");
        }}
      />
      <View style={styles.Btns}>
        <Button
          title="Generate"
          onPress={() => {
            props.navigation.navigate("BookingListingAdmin");
          }}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
