/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { default as React, FunctionComponent } from "react";
import { StatusBar, View } from "react-native";
import { persistStore } from "redux-persist";
import { Add } from "./src/pages/Add";
import { AdminConfirmation } from "./src/pages/AdminConfirmation";
import { Alternate } from "./src/pages/Alternate";
import BookingListingAdmin from "./src/pages/BookingListingAdmin";
import { Cal } from "./src/pages/components/Cal";
import { CalSlot } from "./src/pages/components/CalSlot";
import { Confirmation } from "./src/pages/Confirmation";
import { Details } from "./src/pages/Details";
import { First } from "./src/pages/First";
import { GenerateWidget } from "./src/pages/GenerateWidget";
import { Index } from "./src/pages/Index";
import { Login } from "./src/pages/Login";
import { PaymentStep } from "./src/pages/PaymentStep";
import { PreConfirmation } from "./src/pages/PreConfirmation";
import { Profile } from "./src/pages/Profile";
import { ReBooking } from "./src/pages/ReBooking";
import { Test } from "./src/pages/Test";
import { TimeSlot } from "./src/pages/TimeSlot";
import { Verification } from "./src/pages/Verification";
import { configureStore } from "./src/state-mgmt/store"; //Import the store

const Stack = createStackNavigator();
export const RootStack = (props = {}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Index" component={Index} />
      <Stack.Screen name="Test" component={Test} />
      <Stack.Screen name="First" component={First} />
      <Stack.Screen name="Alternate" component={Alternate} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="TimeSlot" component={TimeSlot} />
      <Stack.Screen name="Add" component={Add} />
      <Stack.Screen name="Cal" component={Cal} />
      <Stack.Screen name="Details" component={Details} />
      <Stack.Screen name="Verification" component={Verification} />
      <Stack.Screen name="CalSlot" component={CalSlot} />
      <Stack.Screen name="PaymentStep" component={PaymentStep} />
      <Stack.Screen name="Confirmation" component={Confirmation} />
      <Stack.Screen name="AdminConfirmation" component={AdminConfirmation} />
      <Stack.Screen name="PreConfirmation" component={PreConfirmation} />
      <Stack.Screen
        name="BookingListingAdmin"
        component={BookingListingAdmin}
      />
      <Stack.Screen name="GenerateWidget" component={GenerateWidget} />
      <Stack.Screen name="ReBooking" component={ReBooking} />
    </Stack.Navigator>
  );
};

const store = configureStore();
const persistor = persistStore(store);

const App: FunctionComponent = () => (
  <>
    <StatusBar
      backgroundColor={"#FFFFFF"}
      barStyle="light-content"
      hidden={false}
    />

    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <RootStack />
      </NavigationContainer>
    </View>
  </>
);

export default App;
