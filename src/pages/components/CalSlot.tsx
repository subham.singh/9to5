/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { View, Button, StyleSheet } from "react-native";
import { Calendar } from "react-native-calendars";

export const CalSlot = (props) => {
  return (
    <>
      <Calendar
        style={{
          height: 550,
          paddingTop: 140,
          paddingLeft: 20,
          paddingRight: 20,
          paddingBottom: 30,
        }}
        theme={{
          backgroundColor: "#ffffff",
          calendarBackground: "#ffffff",
          textSectionTitleColor: "black",
          textSectionTitleDisabledColor: "#d9e1e8",
          selectedDayBackgroundColor: "#00adf5",
          selectedDayTextColor: "#ffffff",
          todayTextColor: "#00adf5",
          dayTextColor: "#2d4150",
          textDisabledColor: "#d9e1e8",
          dotColor: "#00adf5",
          selectedDotColor: "#ffffff",
          arrowColor: "black",
          disabledArrowColor: "black",
          monthTextColor: "black",
          indicatorColor: "black",
          textDayFontFamily: "monospace",
          textMonthFontFamily: "monospace",
          textDayHeaderFontFamily: "monospace",
          textDayFontWeight: "300",
          textMonthFontWeight: "bold",
          textDayHeaderFontWeight: "300",
          textDayFontSize: 16,
          textMonthFontSize: 16,
          textDayHeaderFontSize: 16,
        }}
        minDate={"2020-11-01"}
        maxDate={"2021-01-30"}
        onDayPress={(day) => {
          console.log("selected day", day);
        }}
        onDayLongPress={(day) => {
          console.log("selected day", day);
        }}
        monthFormat={"MMM yyyy"}
        onMonthChange={(month) => {
          console.log("month changed", month);
        }}
        hideExtraDays={true}
        disableMonthChange={true}
        firstDay={1}
        hideDayNames={false}
        showWeekNumbers={false}
        onPressArrowLeft={(subtractMonth) => subtractMonth()}
        onPressArrowRight={(addMonth) => addMonth()}
        markingType={"period"}
        markedDates={{
          "2020-12-13": { marked: true, dotColor: "#50cebb" },
          "2020-12-14": { marked: true, dotColor: "#50cebb" },
          "2020-12-21": {
            startingDay: true,
            color: "#50cebb",
            textColor: "white",
          },
          "2020-12-22": { color: "#70d7c7", textColor: "white" },
          "2020-12-23": {
            color: "#70d7c7",
            textColor: "white",
            marked: true,
            dotColor: "white",
          },
          "2020-12-24": { color: "#70d7c7", textColor: "white" },
          "2020-12-25": {
            endingDay: true,
            color: "#50cebb",
            textColor: "white",
          },
        }}
      />
      <View style={styles.Btns}>
        <Button
          title="Create Slot"
          onPress={() => {
            props.navigation.navigate("GenerateWidget");
          }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
