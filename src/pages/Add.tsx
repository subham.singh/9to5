/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export const Add = (props) => {
  const addons = ["Masks", "Glooves", "Sanitizers"];

  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      addons: {
        type: "array",
        items: {
          type: "string",
        },
      },
    },
  });

  const _uiSchema = {
    addons: {
      "ui:title": "Accesories You Need",
      "ui:options": {
        addable: false,
        orderable: false,
        removable: false,
        minimumNumberOfItems: addons.length,
      },
      items: {
        "ui:iterate": (i, { values }) => ({
          "ui:title": false,
          "ui:widget": "checkbox",
          "ui:widgetProps": {
            text: addons[i],
            value: addons[i],
            checked: (values.addons || []).includes(addons[i]),
          },
        }),
      },
    },
  };

  return (
    <ScrollView>
      <View style={{ justifyContent: "center", flex: 1, alignItems: "center" }}>
        <JsonForm
          schema={_schema}
          uiSchema={_uiSchema}
          _onSuccess={(e) => {
            props.navigation.navigate("PaymentStep");
          }}
        />
        <View style={styles.Btns}>
          <Button
            title="Continue"
            onPress={() => {
              props.navigation.navigate("PaymentStep");
            }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
