/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { View, Button } from "react-native";

export const ReBooking = (props) => {
  return (
    <View
      style={{ justifyContent: "space-around", flex: 1, alignItems: "center" }}
    >
      <Button
        title="Create Booking"
        onPress={() => {
          props.navigation.navigate("Cal");
        }}
      />
    </View>
  );
};
